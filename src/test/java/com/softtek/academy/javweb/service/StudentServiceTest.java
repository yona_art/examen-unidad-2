package com.softtek.academy.javweb.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.configuration.JDBCConfiguration;
import com.softtek.academy.javaweb.service.StudentService;



/**
 * StudentServiceTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class StudentServiceTest {


    @Autowired
    private StudentService studentService;

    @Test
    public void testGetById(){
        // setup
        int id = 8;
        Person expectedPerson = new  Person(8, "Juanito", "30");
        //execute
        Person actualPerson =  studentService.getById(id);
        //validate
        assertNotNull(actualPerson);
        assertEquals(expectedPerson.getName(), actualPerson.getName());
    }
    
}
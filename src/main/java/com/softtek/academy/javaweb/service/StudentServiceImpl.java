package com.softtek.academy.javaweb.service;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.StudentDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * StudentServiceImpl
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    @Qualifier("studentRepository")
    private StudentDao studentDao;

    @Override
    public List<Person> getAll() {
        return studentDao.getAll();
    }

    @Override
    public Person getById(int id) {
        return studentDao.getById(id);
    }

    @Override
    @Transactional
    public void delete(int id) {
        studentDao.delete(id);
    }

    @Override
    @Transactional
    public void update(int id, Person person) {
       studentDao.update(id, person);
    }

    @Override
    @Transactional
    public void addUser(Person person) {
        studentDao.addUser(person);
    }

}
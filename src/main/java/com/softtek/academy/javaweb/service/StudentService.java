package com.softtek.academy.javaweb.service;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;

/**
 * StudentService
 */
public interface StudentService {

    List<Person> getAll();
    Person getById(int id);
    void delete(int id);
    void update(int id, Person person);
    void addUser(Person person);
    
}
package com.softtek.academy.javaweb.dao;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;

/**
 * StudentDao
 */
public interface StudentDao {

    List<Person> getAll();
    Person getById(int id);
    void delete(int id);
    void update(int id, Person person);
    void addUser(Person person);
}
package com.softtek.academy.javaweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.softtek.academy.javaweb.beans.Person;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * StudentRepository
 */
@Repository("studentRepository")
public class StudentRepository implements StudentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Person> getAll() {
        return (List<Person>) entityManager.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    }

    @Override
    public Person getById(int id) {

        return entityManager.find(Person.class, id);
    }

    @Override
    public void delete(int id) {
        Person p = entityManager.find(Person.class, id);
        entityManager.remove(p);
    }

    @Override
    public void update(int id, Person person) {
        Person p = entityManager.find(Person.class, id);
        p.setName(person.getName());
        p.setAge(person.getAge());
    }

    @Override
    public void addUser(Person person) {
        entityManager.persist(person);
    }

}
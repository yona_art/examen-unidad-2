package com.softtek.academy.javaweb.controller;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class PersonController {

    @Autowired
    private StudentService service;


    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public ModelAndView student() {
        return new ModelAndView("person", "person", new Person());
    }

    @RequestMapping(value="/listperson", method = RequestMethod.GET)
    public ModelAndView getMethodName() {
        List<Person> list = service.getAll();
        ModelAndView page = new ModelAndView("list");
        page.addObject("list", list);
        return page;
    }
    

    @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public ModelAndView addPerson(@ModelAttribute("person") Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        service.addUser(person);
        List<Person> list = service.getAll();
        ModelAndView page = new ModelAndView("list");
        page.addObject("list", list);
        return page;
    }

    @RequestMapping(value = "/personUpdateForm/{id}", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable(value="id") int id) {
        Person person = service.getById(id);
        return new ModelAndView("updateForm", "update", person);
    }

    @RequestMapping(value = "/UpdatePerson", method = RequestMethod.POST)
    public ModelAndView updatePerson(@ModelAttribute("person") Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        service.update(person.getId(), person);
        List<Person> list = service.getAll();
        ModelAndView page = new ModelAndView("list");
        page.addObject("list", list);
        return page;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView getMethodName(@PathVariable(value="id") int id) {
       service.delete(id);
       List<Person> list = service.getAll();
       ModelAndView page = new ModelAndView("list");
       page.addObject("list", list);
       return page;
    }

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
</head>

<body>
    <table>
        <thead>
            <tr>ID</tr>
            <tr>Name</tr>
            <tr>Age</tr>
            <tr>Eliminar</tr>
        </thead>

        <tbody>
            <c:forEach items="${list}" var="person">
            <tr>
                <td><a href="/MVCSpring/personUpdateForm/${person.getId()}">${person.getId()}</a></td>
                <td>${person.getName()}</td>
                <td>${person.getAge()}</td>
                <td><a href="/MVCSpring/delete/${person.getId()}">Eliminar</a></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    
    <a href="/MVCSpring">Index</a>
</body>

</html>